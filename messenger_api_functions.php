<?php

//****************************************************************************
//*******************Functions************************************************
//****************************************************************************

function sendList($recipient, $options, $identifier, $buttonName, $limit, $notification_type = "NO_PUSH"){
  $a = '{
    "recipient":{
      "id":"'.$recipient.'"
    }, "message": {
      "attachment": {
          "type": "template",
          "payload": {
              "template_type": "list",
              "top_element_style": "compact",
              "elements": [';

  foreach ($options as $option) {
  $keys = array_keys($option);

    $a .='{
          "title": "'.addcslashes($option[$keys[1]], '"\\/').'",';

          if (isset($option[$keys[3]])) {
            $a .= '"image_url":"'.$option[$keys[3]].'",';
          }

        $a .='
        "subtitle": "'.addcslashes($option[$keys[2]], '"\\/').'",
          "buttons": [
              {
                  "title": "'.addcslashes($buttonName, '"\\/').'",
                  "type": "postback",
                  "payload": "ShowDetails_'.$identifier.'_'.addcslashes($option[$keys[0]], '"\\/').'"
              }
          ]
      },';
  }

  $a = rtrim($a, ",");
  $a .= ' ],
               "buttons": [
                  {
                      "title": "Mehr",
                      "type": "postback",
                      "payload": "ViewMore_'.$identifier.'_'.$limit.'"
                  }
              ]
          }
      }
  },';

  if (isset($notification_type)){
    $a .= '"notification_type":"'.$notification_type.'"';
  }
  else {
    $a = rtrim($a, ",");
  }

  $a .='}';

  return sendToAPI($a);
}

function quickReplyWithImage($recipient, $text, $options, $identifier){

  $a = '{
    "recipient":{
      "id":"'.$recipient.'"
    },
    "message":{
      "text":"'.addcslashes($text, '"\\/').'",
      "quick_replies":[';

      foreach ($options as $option) {
        $keys = array_keys($option);
        $a .= '
              {
                "content_type":"text",
                "title":"'.addcslashes($option[$keys[1]], '"\\/').'",
                "payload":"quickreply_'.$identifier.'_'.$option[$keys[0]].'",
                "image_url":"'.$option[$keys[2]].'"
              },';
      }

  $a = rtrim($a, ",");

  $a .='
            ]
          }
        }';


  return sendToAPI($a);
}

function quickReplyWithText($recipient, $text, $options, $identifier){

  $a = '{
    "recipient":{
      "id":"'.$recipient.'"
    },
    "message":{
      "text":"'.addcslashes($text, '"\\/').'",
      "quick_replies":[';

      foreach ($options as $option) {
        $keys = array_keys($option);
        $a .= '
              {
                "content_type":"text",
                "title":"'.addcslashes($option[$keys[1]], '"\\/').'",
                "payload":"quickreply_'.$identifier.'_'.addcslashes($option[$keys[0]], '"\\/').'"
              },';
      }

  $a = rtrim($a, ",");

  $a .='
            ]
          }
        }';


  return sendToAPI($a);
}

function sendGenericURLButtonWithShare($recipient, $options, $notification_type = NULL, $noWebURLButton = NULL){


  $a =  '{
      "recipient":{
        "id":"'.$recipient.'"
        },
        "message":{
      "attachment":{
        "type":"template",
        "payload":{
          "template_type":"generic",
          "elements":[';
    if(is_array($options[0])){
      foreach ($options as $option) {
        $keys = array_keys($option);
            $a .= '{
              "title":"'.addcslashes($option[$keys[0]], '"\\/').'",
              "item_url":"'.$option[$keys[1]].'",';

              if (isset($option[$keys[2]])) {
                $a .= '"image_url":"'.$option[$keys[2]].'",';
              }

              $a .= '
              "subtitle":"'.addcslashes($option[$keys[3]], '"\\/').'",
              "buttons":[';
                if($option[$keys[1]] != "" && !isset($noWebURLButton)){
                  $a .= '{
                    "type":"web_url",
                    "url":"'.$option[$keys[1]].'",
                    "title":"'.addcslashes($option[$keys[4]], '"\\/').'"
                      },';
                }
                $a .='
                {
                  "type":"element_share"
                }
              ]
            },';
        }
        $a = rtrim($a, ",");
      }
      else {
        $keys = array_keys($options);
            $a .= '{
              "title":"'.addcslashes($options[$keys[0]], '"\\/').'",
              "item_url":"'.$options[$keys[1]].'",';

              if (isset($options[$keys[2]])) {
                $a .= '"image_url":"'.$options[$keys[2]].'",';
              }

              $a .= '
              "subtitle":"'.addcslashes($options[$keys[3]], '"\\/').'",
              "buttons":[';
                if($options[$keys[1]] != ""  && !isset($noWebURLButton)){
                  $a .= '{
                    "type":"web_url",
                    "url":"'.$options[$keys[1]].'",
                    "title":"'.addcslashes($options[$keys[4]], '"\\/').'"
                      },';
                }
                $a .='
                {
                  "type":"element_share"
                }
              ]
            }';
      }

      $a .=']}}},';

      if (isset($notification_type)){
        $a .= '"notification_type":"'.$notification_type.'"';
      }
      else {
        $a = rtrim($a, ",");
      }

      $a .='}';

    return sendToAPI($a);

}

function sendTextMessage($recipient, $text, $notification_type = "NO_PUSH"){

  $a =  '{
      "recipient":{
          "id":"'.$recipient.'"
      },
      "message":{
          "text":"'.addcslashes($text, '"\\/').'"
      },';

      if (isset($notification_type)){
        $a .= '"notification_type":"'.$notification_type.'"';
      }
      else {
        $a = rtrim($a, ",");
      }

      $a .='}';

  return sendToAPI($a);

}

function typingOn($recipient) {

  $a = '{
    "recipient":{
    	"id":"'.$recipient.'"
    },
    "sender_action":"typing_on"}';

    return sendToAPI($a);

}

function sendURLButtonButton($recipient, $options, $notification_type){

  $a =  '{
      "recipient":{
        "id":"'.$recipient.'"
      },
      "message":{
        "attachment":{
          "type":"template",
          "payload":{
            "template_type":"button",
            ';


            foreach ($options as $option) {
              $keys = array_keys($option);

              $option[$keys[0]] = addcslashes($option[$keys[0]], '"\\/');
              $a .= '"text":"'.$option[$keys[0]].'",
              "buttons":[
                {
                  "type":"web_url",
                  "url":"'.$option[$keys[1]].'",
                  "title":"'.addcslashes($option[$keys[2]], '"\\/').'"
                },';
            }
            $a .='
            ]
          }
        }
      },';
      if (isset($notification_type)){
        $a .= '"notification_type":"'.$notification_type.'"';
      }
      else {
        $a = rtrim($a, ",");
      }

      $a .='}';

    return sendToAPI($a);

}

function sendButtonWithMultiButtons($recipient, $options, $identifier, $notification_type){

  $a =  '{
      "recipient":{
        "id":"'.$recipient.'"
      },
      "message":{
        "attachment":{
          "type":"template",
          "payload":{
            "template_type":"button",
            "text":"'.addcslashes($text, '"\\/').'",
            "buttons":[';


            foreach ($options as $option) {
              $keys = array_keys($option);
              $a .= '
                {
                  "type":"postback",
                  "title":"'.$option[$keys[1]].'",
                  "payload":"btn_'.$identifier.'_'.$option[$keys[0]].'"
                },';
            }
            $a = rtrim($a, ",");

            $a .='
            ]
          }
        }
      },
      "notification_type":"'.$notification_type.'"
    }';

    return sendToAPI($a);
  }

function sendButton($recipient, $text, $buttonText, $payload, $notification_type){

  $a =  '{
      "recipient":{
        "id":"'.$recipient.'"
      },
      "message":{
        "attachment":{
          "type":"template",
          "payload":{
            "template_type":"button",
            "text":"'.addcslashes($text, '"\\/').'",
            "buttons":[
              {
                "type":"postback",
                "title":"'.addcslashes($buttonText, '"\\/').'",
                "payload":"'.$payload.'"
              }
            ]
          }
        }
      },
      "notification_type":"'.$notification_type.'"
    }';
    return sendToAPI($a);
}

function sendImage($recipient, $url){

  $a = '{
  "recipient":{
    "id":"'.$recipient.'"
  },
  "message":{
    "attachment":{
      "type":"image",
      "payload":{
        "url":"'.$url.'"
      }
    }
  }
  }';
  return sendToAPI($a);
}

function sendAudio($recipient, $url){

  $a = '{
  "recipient":{
    "id":"'.$recipient.'"
  },
  "message":{
    "attachment":{
      "type":"audio",
      "payload":{
        "url":"'.$url.'"
      }
    }
  }
  }';

  return sendToAPI($a);

}

function sendVideo($recipient, $url){

  $a = '{
  "recipient":{
    "id":"'.$recipient.'"
  },
  "message":{
    "attachment":{
      "type":"video",
      "payload":{
        "url":"'.$url.'"
      }
    }
  }
  }';

  return sendToAPI($a);

}

function sendFile($recipient, $url){

  $a = '{
  "recipient":{
    "id":"'.$recipient.'"
  },
  "message":{
    "attachment":{
      "type":"file",
      "payload":{
        "url":"'.$url.'"
      }
    }
  }
  }';

  return sendToAPI($a);

}

//Initiate cURL.
function initcURL($apiurl){

  $ch = curl_init($apiurl);


  return $ch;


}
//API call
function sendToAPI($jsonDataEncoded) {
  global $ch;

  //Tell cURL that we want to send a POST request.
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  //Attach our encoded JSON string to the POST fields.
  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
  //Set the content type to application/json
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //Execute the request
  $curlresult = curl_exec($ch);

  if (strpos($curlresult, 'error') !== false){
    mail('cgh787@gmail.com', "curlresult", $curlresult."\n \n".$jsonDataEncoded);
  }

  return $curlresult;
}

function getUserProfile($userID, $access_token){

  $ch = curl_init("https://graph.facebook.com/v2.6/{$userID}?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token={$access_token}");
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  //Set the content type to application/json
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  //Execute the request
  $curlresult = curl_exec($ch);
  //mail('cgh787@gmail.com', "curlresult", $curlresult);
  return json_decode($curlresult, true);
  curl_close($ch);

}

//Close cURL
function closecURL($ch){
  curl_close($ch);
}

?>
